package net.wizapps.kotlin.k_Collections

import net.wizapps.kotlin.e_DataClasses.Car

fun main(args: Array<String>) {
    collectionsFunctions()
    filterSample()
    mapSample()
    combineFewFunctions()
}

fun collectionsFunctions() {
    val seasons = listOf("winter", "spring", "summer", "autumn")
    val colors = listOf("red", "blue", "green")
    val duplicates = listOf("One", "Two", "Two", "Three", "One", "Four", "One")
    val numbers = listOf(1, 2, 3, 4, 5, 6, 7)

    /** Get first or last item in list */
    println(seasons.first())
    println(seasons.last())

    /** Reverse list */
    println(colors.asReversed())

    /** Get item from list if exist, if not return null */
    println(numbers.getOrNull(11))

    /** Get item from list if exist, if not return default value */
    println(numbers.getOrElse(15) { 100 })

    /** Get mix & max value in list, if not return null */
    println(numbers.minOrNull())
    println(numbers.maxOrNull())

    /** Combine two list by pairs(look at the output) */
    println(seasons.zip(colors))

    /** Merge two list in two list collection(look at the output) */
    val mergedLists = listOf(seasons, colors)
    println(mergedLists)

    /** Combine two list as one(look at the output) */
    val combinedList = seasons + colors
    println(combinedList)

    /** Remove duplicates from list */
    println(duplicates.distinct())

    /** If all items in collection match this condition */
    println(getCarsMap().all { it.value.year > 2015 })

    /** If any item in collection match this condition */
    println(getCarsMap().any { it.value.color == "Red" })

    /** How many items in collection match this condition */
    println(getCarsMap().count { it.value.year > 2005 })

    /** Find first item in collection that match this condition, didn't work for maps */
    val cars = getCarsMap().values
    println(cars.find { it.year > 2005 })

    /** Sort list by parameter */
    println(cars.sortedBy { it.model })

    /** Sort map by key */
    println(getCarsMap().toSortedMap())
}

fun filterSample() {
    val mutableMap = mutableMapOf( // or -> hashMapOf()
            "Frodo" to Car("Audi", "Blue", 2008),
            "Sam" to Car("BMW", "Black", 2011),
            "Pippin" to Car("Ford", "White", 2003))

    /**
     * Filter function did not change the collection,
     * it's useful when you need do get some values from collection,
     * but not change it, for changing "map" function is exist
     */
    println(mutableMap.filter { it.value.year == 2008 })
    println(mutableMap)
}

fun mapSample() {
    /** Java like code */
    val ints = arrayOf(1, 2, 3, 4, 5)
    val intsPlusTen = mutableListOf<Int>()
    for (i in ints) {
        intsPlusTen.add(i + 10)
    }
    println(intsPlusTen)

    /**
     * Kotlin like code
     * Map function is changing the collection, returns List<R>
     */
    val kotlinInts = ints.map { it + 10 }
    println(kotlinInts)

    /** If we want to get list of some data from Map with "map" function */
    val yearsList = getCarsMap().map { it.value.year }
    println(yearsList.javaClass)
    println(yearsList)
}

fun combineFewFunctions() {
    val newCars = getCarsMap()
            .filter { it.value.year >= 2015 }
            .map {
                it.value.model = "NEW ${it.value.model}"
                /**
                 * Last parameter in map, in case with HashMap, its type of List that will be return.
                 * In our case its a ArrayList<String>
                 */
                it.value.model
            }
    println(newCars)
}

fun getCarsMap() = mutableMapOf(
        17 to Car("Audi", "Blue", 2008),
        21 to Car("BMW", "Black", 2015),
        3 to Car("Ford", "White", 2003),
        44 to Car("Mercedes", "Brown", 2017),
        5 to Car("Volvo", "Silver", 2005),
        61 to Car("Ferrari", "Red", 2018),
        7 to Car("VW", "Green", 1998))