package net.wizapps.kotlin.k_Collections

fun main(args: Array<String>) {
    setsType()
}

fun setsType() {
    /** Immutable Set sample */
    val immutableSet = setOf(22, 15, 7, 3, 19, 33, 144)
    println(immutableSet)

    /** Mutable Set sample */
    val mutableSet = mutableSetOf( // or -> hashSetOf()
            Worker("Ihor", "Choliy", 56000),
            Worker("Will", "Smith", 16000),
            Worker("Indiana", "Jones", 43000))

    /**
     * Adding items in mutable set
     * If you don't use data classes for set, you should override hashCode() & equals()
     * Will not add this object because it's identical to one that is already in the setList
     */
    mutableSet.add(Worker("Will", "Smith", 16000))
    println(mutableSet)
}