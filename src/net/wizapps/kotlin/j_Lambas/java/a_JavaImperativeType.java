package net.wizapps.kotlin.j_Lambas.java;

import java.util.Comparator;
import java.util.List;

public class a_JavaImperativeType {

    public static void main(String[] args) {
        List<Person> persons = Person.getPersons();

        // Step 1: Sort list by last name
        persons.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        });

        // Step 2: Create a method that prints all elements in list
        printAllElements(persons);

        // Step 3: Create a method that prints all people that have name beginning with "C"
        printConcretePerson(persons, new Condition() {
            @Override
            public boolean isTrueCondition(Person person) {
                return person.getLastName().startsWith("C");
            }
        });
    }

    private static void printAllElements(List<Person> persons) {
        System.out.println("Sorted list of Persons:");
        for (Person person : persons) {
            System.out.println(person);
        }
    }

    private static void printConcretePerson(List<Person> persons, Condition condition) {
        System.out.println("\nPersons witch starts with concrete letter:");
        for (Person person : persons) {
            if (condition.isTrueCondition(person)) {
                System.out.println(person);
            }
        }
    }

    interface Condition {
        boolean isTrueCondition(Person person);
    }
}