package net.wizapps.kotlin.j_Lambas.java;

import java.util.Arrays;
import java.util.List;

public class Person {

    private String firstName;
    private String lastName;
    private int age;

    private Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person: [First name: " + firstName + ", Last name: " + lastName + ", Age: " + age + "]";
    }

    static List<Person> getPersons() {
        return Arrays.asList(
                new Person("Charlie", "Dickens", 60),
                new Person("Lewis", "Carroll", 98),
                new Person("Thomas", "Carlyle", 87),
                new Person("Charlotte", "Bronte", 92),
                new Person("Matthew", "Arnold", 89));
    }
}