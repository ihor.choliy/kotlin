package net.wizapps.kotlin.j_Lambas

import net.wizapps.kotlin.a_Basics.Employee

fun main(args: Array<String>) {
    minimumValueLambda()
    localVariablesInLambda(17)

    /** If function is top level you could use it only with double quotes */
    run(::topLevelFunction)
    println(countTo100())
    println(countTo100With())
    println(countTo100Apply())
}

fun minimumValueLambda() {
    /**
     * It means that "e" it's a element in collection, and every element we will compare by "id".
     * We could rename "e" to any name we want
     * If the Lambda is the last parameter in the function, You could add it without brackets */
    println(getEmployees().minByOrNull { e -> e.id })

    /**
     * We could specified the type of Object, but this is not necessary at all,
     * the compiler understands the type by itself */
    println(getEmployees().minByOrNull { employee: Employee -> employee.id })

    /**
     * When it's only one parameter in Lambda You could use "it" instead,
     * this is completely the same expression as two above */
    println(getEmployees().minByOrNull { it.id })

    /** Sample with member reference, without Lambda, like in Java */
    println(getEmployees().minByOrNull(Employee::id))
}

/** You can use function parameter in Lambda */
fun localVariablesInLambda(number: Int) {
    var text: String
    run {
        text = "Bla bla"
        println("Integer: $number, String: $text")
    }
}

fun topLevelFunction() = println("I'm in a top level function")

/** Example of function without Lambda */
fun countTo100(): String {
    val numbers = StringBuilder()
    for (i in 1..99) {
        numbers.append(i)
        numbers.append(", ")
    }
    numbers.append(100)
    return numbers.toString()
}

/**
 * Example of function with Lambda and "with" function
 * In "with" lambda function you could edit parameter -> StringBuilder() without calling instance
 */
fun countTo100With() =
        with(StringBuilder()) {
            for (i in 1..99) {
                append("$i, ")
            }
            append(100)
            toString()
        }

/**
 * Lambda "apply" function
 * "apply" lambda function always returns instance of the object witch is call it
 */
fun countTo100Apply() =
        StringBuilder().apply {
            for (i in 1..99) {
                append("$i, ")
            }
            append(100)
        }.toString()

fun getEmployees() = listOf(
        Employee("Ihor", 7),
        Employee("Andrey", 1),
        Employee("John", 5),
        Employee("Kate", 3),
        Employee("Mary", 9))