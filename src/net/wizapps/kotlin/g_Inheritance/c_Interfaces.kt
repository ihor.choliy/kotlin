package net.wizapps.kotlin.g_Inheritance

fun main(args: Array<String>) {
    val myClass = MyClass()
    println(myClass.numberNotInitialized)
    println(myClass.initializedNumber)
    println(myClass.superInterfaceFunction("LoL"))
    println(myClass.subInterfaceFunction(33))
}

interface MyInterface {
    fun superInterfaceFunction(string: String): String
}

interface MySubInterface : MyInterface {

    /** In Kotlin interfaces variables may not be initialized */
    val numberNotInitialized: Int

    /** If we want to have initialized variables in Kotlin interfaces we should initialized them in get() function */
    val initializedNumber: Double get() = 132.7

    fun subInterfaceFunction(integer: Int): Double
}

class MyClass : MySubInterface {

    override val numberNotInitialized: Int = 111

    override fun superInterfaceFunction(string: String): String {
        return "superInterfaceFunction: $string"
    }

    override fun subInterfaceFunction(integer: Int): Double {
        return integer * 12.toDouble()
    }
}