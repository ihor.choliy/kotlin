package net.wizapps.kotlin.g_Inheritance

fun main(args: Array<String>) {
    val printer = LaserPrinter("HP 1220", 20)
    printer.printModel()
    println("Recommended price: ${printer.bestSellingPrice()}")
}

abstract class Printer(private val modelName: String) {

    abstract fun bestSellingPrice(): Double

    /**
     * All functions, classes, variables in Kotlin by default is "public final",
     * that's why for overriding parent functions we should add "open" modifier
     */
    open fun printModel() = println("Printer model is: $modelName")
}

class LaserPrinter(modelName: String, private val pagesPerMinute: Int) : Printer(modelName) {

    override fun bestSellingPrice(): Double = 129.99

    /**
     * "override" implicitly means that current fun is "open".
     * If we don't want to give possibility for overriding this function for other subclasses,
     * we should add "final" modifier
     */
    override fun printModel() {
        super.printModel()
        println("Pages per minute: $pagesPerMinute")
    }
}