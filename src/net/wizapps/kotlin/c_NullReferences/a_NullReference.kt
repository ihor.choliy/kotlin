package net.wizapps.kotlin.c_NullReferences

fun main(args: Array<String>) {
    nullReference()
    asserting()
    letFunction()
    safeCasting()
    arrayOfNulls()
}

/**
 * Can't assign null in Kotlin by default:
 * val nullString: String = null
 */
fun nullReference() {
    val nullableString1 = "This isn't null"
    if (nullableString1 != null) {
        nullableString1.toUpperCase()
    }

    /**
     * Simpler way for null checking:
     * ? question mark says that variable can be null
     */
    nullableString1?.toUpperCase()

    /** Another sample */
    val nullableString2: String? = null
    println("What happened when do the follows: ${nullableString2?.toUpperCase()}")

    /**
     * Java null checking sample:
     * Country country = Country.getUserCountry();
     * if (country != null) {
     *     City city = country.getUserCity();
     *     if (city != null) {
     *         String address = city.getUserAddress();
     *     }
     * }
     *
     * Same code in Kotlin:
     * val userAddress: String? = Country.userCountry?.userCity?.userAddress ?: "Default address"
     */

    /** If value returns null ?: "Elvis" operator will set default value */
    val nullableString3 = nullableString2 ?: "Default value"
    println(nullableString3)
}

fun asserting() {
    /**
     * NOT null assertion.
     * !! operator says to compiler that 100% no null will be there
     */
    val nullableString: String? = null
    val notNullAssertionString = nullableString!!.toUpperCase() // exception will be thrown

    /** Redundant code of upper operation */
    if (notNullAssertionString == null) {
        throw NullPointerException()
    } else {
        notNullAssertionString.toUpperCase()
    }

    /** Assertion NullPointerException visualizing */
    val nullableString1: String? = null
    val nullableString2 = nullableString1!! // here will be thrown NullPointerException(where assertion operator is called)
    val nullableString3 = nullableString2.toUpperCase()
}

fun letFunction() {
    val someString: String? = "bla bla bla..."
    /** let{} function is called when object not null(?) */
    someString?.let {
        it.toUpperCase() // "it" is reference to object(in our case it's String)
        println(it)
    }
}

fun safeCasting() {
    val someArray = arrayOf(1, 2, 3, 4)
    val someString = someArray as? String // "as" with question mark(?) cast object only if it possible
    println(someString) // no exception will be thrown
    println(someString?.toUpperCase())
}

fun arrayOfNulls() {
    val arrayOfNulls = arrayOfNulls<Int?>(5)
    arrayOfNulls.forEach { println(it) } // no exception will be thrown
}