package net.wizapps.kotlin.d_Constructors

fun main(args: Array<String>) {
    EmployeeKotlin("John", "Brisket", 54)

    EmployeeSimplerKotlin("Eddy", "Smith", 48)
    EmployeeSimplerKotlin("Bob", "Martin", 55).getData()

    MultipleConstructorsEmployee("Anna", "Rothschild", false).showInfo()

    SingleConstructorEmployee("Julia", "Windsor").showInfo()
    SingleConstructorEmployee("Frodo", "Baggins", false).showInfo()

    EmptyConstructorEmployee().showInfo()
    EmptyConstructorEmployee("Rockefeller").showInfo()
    EmptyConstructorEmployee("Ihor", "Choliy", true).showInfo()

    WithoutConstructorClass()
}

/**
 * All classes in Kotlin are public & final by default.
 * When you added "constructor" key before brackets in Kotlin, this mean that it's a primary constructor.
 * You can add this keyword or not.
 * Primary constructor it's a constructor that declares after class name
 */
class EmployeeKotlin constructor(firstName: String, secondName: String, age: Int) {

    private val firstName: String
    private val secondName: String

    /**
     * init{} block it's a body for primary constructor.
     * You need init{} block only if you want to make some logic inside primary constructor
     */
    init {
        this.firstName = firstName.toLowerCase().trim()
    }

    /** You could create multiple init{} blocks for organization of code, they will be running from top to bottom */
    init {
        this.secondName = secondName.toLowerCase().trim()
        getFullEmployeeInfo()
    }

    /** You could init variables without init{} block */
    private val age: Int = age.hashCode().plus(10)

    private fun getFullEmployeeInfo() {
        println("Employee full name: $firstName $secondName\nAge: $age")
    }
}

/** Simpler way to create a class */
class EmployeeSimplerKotlin(val firstName: String, val secondName: String, val age: Int) {

    init {
        println("Employee full name: $firstName $secondName\nAge: $age")
    }

    fun getData() = println(listOf(firstName, secondName, age))
}

/** If You want to change visibility of the constructor You should add "constructor" keyword with private modifier */
class SomeClass private constructor()

/** Multiple constructors sample */
class MultipleConstructorsEmployee(private val firstName: String, private val secondName: String) {

    var fullTime: Boolean = true

    /**
     * New variables that were not in primary constructor, didn't creates in secondary constructors.
     * You should add and initialize them by yourself.
     * If you have secondary constructor you should always extends from primary one
     */
    constructor(firstName: String, secondName: String, fullTime: Boolean) : this(firstName, secondName) {
        this.fullTime = fullTime
    }

    fun showInfo() {
        println("$firstName $secondName $fullTime\n")
    }
}

/**
 * Avoid multiple constructors as in above case by adding default value for "fullTime"
 * In this case two different constructors will be available.
 * This is best practice, and more Kotlin like to avoid multiple constructors
 */
class SingleConstructorEmployee(val firstName: String, val secondName: String, var fullTime: Boolean = true) {

    fun showInfo() {
        println("$firstName $secondName $fullTime\n")
    }
}

/** Class with empty primary constructor */
class EmptyConstructorEmployee() {

    private var firstName: String
    private var secondName: String = "Choliy"

    init {
        firstName = "Ihor"
    }

    constructor(secondName: String) : this() {
        this.secondName = secondName
    }

    constructor(firstName: String, secondName: String, toLowerCase: Boolean) : this() {
        if (toLowerCase) {
            this.firstName = firstName.toLowerCase()
            this.secondName = secondName.toLowerCase()
        } else {
            this.firstName = firstName.toUpperCase()
            this.secondName = firstName.toUpperCase()
        }
    }

    fun showInfo() {
        println("$firstName $secondName\n")
    }
}

/** Class without constructor */
class WithoutConstructorClass {

    init {
        val someData = 47.hashCode().div(4).plus(77).div(83).xor(358).times(42)
        println(someData)
    }
}