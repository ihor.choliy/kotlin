package net.wizapps.kotlin.d_Constructors

fun main(args: Array<String>) {
    val player = Player("Elfigor", "Elf", 81)
    player.level = 82
    player.race = "Orc"
    println(player.level)
    println(player.race)
}

/**
 * If we have "val" properties in primary constructor, Kotlin generates getters for us.
 * If we have "var" properties in primary constructor, Kotlin generates getters & setters for us.
 */
class Player(val nickname: String, race: String, level: Int) {

    /**
     * If You want to have custom getters & setters with some extra logic inside,
     * You should call get() & set(value) functions immediately after field declaration,
     * and don't add "val" or "var" before variables in the constructor,
     * but add them before field declaration(var race = race)
     */
    var level = level
        get() {
            return field.div(7).plus(42).hashCode()
        }
        set(value) {
            field = value.xor(11).minus(54).hashCode()
        }

    var race = race
        set(value) {
            field = value
        }
}