package net.wizapps.kotlin.o_JavaClasses;

public class DummyMethods {

    public String isVacationTime(boolean isVacation) {
        return isVacation ? "I'm on vacation!" : "I'm working :(";
    }

    public void printNumbers(int[] numbers) {
        for (int number : numbers) {
            System.out.println(number);
        }
    }

    public void printChars(char[] chars) {
        for (char letter : chars) {
            System.out.println(letter);
        }
    }
}