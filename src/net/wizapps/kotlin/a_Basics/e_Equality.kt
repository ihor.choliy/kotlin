package net.wizapps.kotlin.a_Basics

fun main(args: Array<String>) {
    equalsSample()
}

fun equalsSample() {
    val employeeOne = Employee("Ihor", 1)
    val employeeTwo = Employee("John", 2)
    val employeeThree = Employee("John", 2)

    println(employeeOne == employeeTwo)
    println(employeeTwo == employeeThree)

    /**
     * equals() function in kotlin is the same as "=="
     * Recommended use "==" instead
     */
    println(employeeOne.equals(employeeTwo))
    println(employeeTwo.equals(employeeThree))

    /** If you want to compare reference equality use "===" */
    println(employeeOne === employeeTwo)
    println(employeeTwo === employeeThree)
}