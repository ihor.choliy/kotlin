package net.wizapps.kotlin.a_Basics

fun main(args: Array<String>) {
    castingSample()
}

fun castingSample() {
    val employee = Employee("Ihor", 1)
    var something: Any = employee
    if (something !is Employee) {
        something = something as Employee
        println(something.name)
    }
}