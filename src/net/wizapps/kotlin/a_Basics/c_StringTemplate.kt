package net.wizapps.kotlin.a_Basics

fun main(args: Array<String>) {
    stringSample()
    tripleQuotedString()
}

fun stringSample() {
    val dollarValue = 26.5
    println("Today's dollar value:$$dollarValue")

    val numerator = 10.99
    val denominator = 20.11
    println("The value of $numerator divided by $denominator is: ${numerator / denominator}")

    val employee = Employee("Frodo", 111)
    println("The employees name is ${employee.name}")
}

fun tripleQuotedString() {
    /** Use triple quotes for strings with file path */
    val filePath = """D:\Games\WorldOfWarcraft\wow.exe"""
    println(filePath)

    /**
     * trimMargin() is trimming whitespace characters followed by "marginPrefix".
     * Default "marginPrefix" is "|", if you want to change it you should set in the constructor -> trimMargin("A")
     */
    val multilineString = """first line
        |second line
        |third line
        |fourth line
        |fifth line""".trimMargin()
    println(multilineString)
}