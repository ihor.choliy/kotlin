package net.wizapps.kotlin.a_Basics

fun main(args: Array<String>) {
    val employee = Employee("Ihor", 77)
    println(employee.toString())
    println(employee.hashCode())
}

class Employee(var name: String, val id: Int) {

    override fun equals(other: Any?): Boolean {
        return if (other is Employee) {
            name == other.name && id == other.id
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + id
        return result
    }

    override fun toString(): String {
        return "Employee(name=$name, id=$id)"
    }
}