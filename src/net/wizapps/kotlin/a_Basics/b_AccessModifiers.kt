package net.wizapps.kotlin.a_Basics

fun main(args: Array<String>) {
    val player = Player()
    println(player)
}

/**
 * There is a four access modifiers in Kotlin:
 * private, public, protected, internal
 *
 * - Internal modifier visible only in the same module
 * - If you do not set modifier explicitly, public modifier will be set
 * - Private modifier visible only in the same file
 * - Private modifiers for classes allowed in Kotlin
 */
private class Player