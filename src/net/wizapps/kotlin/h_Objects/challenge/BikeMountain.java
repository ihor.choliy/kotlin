package net.wizapps.kotlin.h_Objects.challenge;

public class BikeMountain extends Bike {

    private int seatHeight;

    public BikeMountain(int cadence, int gear, int speed, int seatHeight) {
        super(cadence, gear, speed);
        this.seatHeight = seatHeight;
    }

    public int getSeatHeight() {
        return seatHeight;
    }

    public void setSeatHeight(int seatHeight) {
        this.seatHeight = seatHeight;
    }

    @Override
    public void bikeDescription() {
        super.bikeDescription();
        System.out.println("The mountain bike has a seat height of " + seatHeight + ".");
    }
}