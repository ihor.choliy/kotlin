package net.wizapps.kotlin.h_Objects

fun main(args: Array<String>) {

    /** For creating anonymous object you should add "object" keyword */
    wantsSomeInterface(object : SomeInterface {
        override fun mustImplement(number: Int): String {
            return "Text from anonymous object with number: $number"
        }
    })
}

interface SomeInterface {
    fun mustImplement(number: Int): String
}

fun wantsSomeInterface(someInterface: SomeInterface) {
    println("Prints from wantsSomeInterface: ${someInterface.mustImplement(22)}")
}