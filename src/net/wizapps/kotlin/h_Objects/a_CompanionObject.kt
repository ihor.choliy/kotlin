package net.wizapps.kotlin.h_Objects

fun main(args: Array<String>) {
    /** With companion declaration */
    println(SomeClass.Companion.getPrivateVal())

    /** Without companion declaration */
    println(SomeClass.getPrivateVal())

    /** Companion object if Factory class usage */
    val object1 = FactoryClass.ObjectFactory.getNormalObject("Frodo", 35)
    val object2 = FactoryClass.getObjectWithRandomAge("Sam")

    object1.getInfo()
    object2.getInfo()
}

class SomeClass {

    /**
     * If you want to have equivalent Java static variables and methods in Kotlin,
     * companion object is good options for this case, from companion object you could get
     * variables or functions without creating instance of the class
     *
     * - only one companion object allowed per class
     * - if needed you could add unique name for companion object
     */
    companion object {
        private val number = 777
        fun getPrivateVal() = number
    }
}

class FactoryClass private constructor(private val name: String, private val age: Int) {

    /**
     * Another uses of companion object for factory classes.
     * You could add custom name for companion object if you want to(ObjectFactory)
     */
    companion object ObjectFactory {

        fun getObjectWithRandomAge(name: String): FactoryClass {
            val randomAge = Math.random() * 100
            return FactoryClass(name, randomAge.toInt())
        }

        fun getNormalObject(name: String, age: Int): FactoryClass = FactoryClass(name, age)
    }

    fun getInfo() = println("Name: $name\nAge: $age")
}