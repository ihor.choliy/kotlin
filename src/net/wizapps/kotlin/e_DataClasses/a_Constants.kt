package net.wizapps.kotlin.e_DataClasses

fun main(args: Array<String>) {
    println(MY_ID)
    println(MY_EMAIL)
    println(MY_SEX)
}

/** You could add constants without a class, and they will be available from everywhere in the project */
const val MY_ID = 31765472526435432
const val MY_EMAIL = "ihor.choliy@gmail.com"
const val MY_SEX = "man"