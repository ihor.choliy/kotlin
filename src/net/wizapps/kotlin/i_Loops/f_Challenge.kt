package net.wizapps.kotlin.i_Loops

fun main(args: Array<String>) {
    challengeLoops()
}

fun challengeLoops() {
    /** Print numbers in range from 5 to 5000 with step 5 */
    for (i in 5..500 step 5) println(i)

    /** Print from -500 to 0 included */
    for (i in -500..0) println(i)

    /** Print the first 15 numbers in Fibonacci sequence */
    var firstValue = 0
    var secondValue = 1
    var total = firstValue + secondValue
    println(firstValue)
    println(secondValue)
    for (i in 0..15 - 2) {
        println(total)
        firstValue = secondValue
        secondValue = total
        total = firstValue + secondValue
    }

    /**
     * Declare a variable (int) and assign it whatever you want
     * Declare a variable (double) and assign it as follows:
     * if (int > 100) assign -22.643
     * if (int < 100) assign 543.7
     * if (int == 100) assign 0.213
     * else assign 35.3235
     *
     * Do all above in one expression.
     * Than print the value
     */
    val number = 100
    var double = if (number > 100) {
        -22.643
    } else if (number < 100) {
        543.7
    } else if (number == 100) {
        0.213
    } else {
        35.3235
    }
    println(double)

    /** Do the the same with "when" */
    double = when {
        number > 100 -> -22.643
        number < 100 -> 543.7
        number == 100 -> 0.213
        else -> 35.3235
    }
    println(double)
}