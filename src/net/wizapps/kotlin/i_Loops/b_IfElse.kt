package net.wizapps.kotlin.i_Loops

fun main(args: Array<String>) {
    ifExpression()
}

fun ifExpression() {
    /**
     * Equivalent of Java ternary operator
     * int number = someCondition ? 11 : 22
     */
    val someCondition = true
    val number = if (someCondition) 11 else 22
    println(number)

    /** More complex example */
    val anotherNumber = if (someCondition) {
        println("if logic")
        33
    } else {
        println("else logic")
        44
    }
    println(anotherNumber)

    /**
     * If "if/else" returns nothing(Unit Object).
     * There isn't void returning type in Kotlin,
     * when expression returns nothing the return type is Unit
     * */
    val nothing = if (someCondition) println("something") else println("nothing")
    println(nothing.javaClass)
}