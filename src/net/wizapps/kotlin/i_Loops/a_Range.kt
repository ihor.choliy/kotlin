package net.wizapps.kotlin.i_Loops

fun main(args: Array<String>) {
    rangeExample()
    stepExample()
    untilExample()
    inExample()
}

fun rangeExample() {
    /** Two dots ".." it's range operator */
    val intRange = 1..7
    val charRange = 'c'..'z'
    val stringRange = "ABC".."XYZ"

    println(6 in 6..147)
    println(5 in intRange)
    println('b' in charRange)
    println("FFF" in stringRange)

    /** Backward range sample */
    val intBackwardRange = 22.downTo(7)
    println(5 in intBackwardRange)

    /** Another way */
    val someRange = intRange.reversed()
    for (i in someRange) println(i)
}

fun stepExample() {
    /** If we want to have a range with the current step */
    val newIntRange = 0..100
    val stepRange = newIntRange.step(5)
    println(5 in stepRange)
    println(3 in stepRange)

    /** Print all ranges */
    for (i in 0..30 step 3) println(i)

    /** Print all ranges backwards */
    for (num in 30 downTo 0 step 3) println(num)
}

fun untilExample() {
    /** Prints all numbers from 5 to 9 except 10 */
    for (i in 5 until 10) println(i)
}

fun inExample() {
    val seasons = arrayOf("winter", "spring", "summer", "autumn")
    val notASeason = "shlinter" in seasons
    println(notASeason)

    val notInRange = 32 !in 1..30
    println(notInRange)
}