package net.wizapps.kotlin.n_JavaInteroperability

import net.wizapps.kotlin.o_JavaClasses.JavaCar

fun main(args: Array<String>) {
    changeParameters()
    passArray()
    callStatic()
    sam()
}

fun changeParameters() {
    val javaCar = getCar()

    /**
     * If we want to change parameter in Java class
     * we should change it in Kotlin style, not like that -> javaCar.setColor("White")
     */
    javaCar.color = "White"
    println(javaCar)
}

fun passArray() {
    getCar().printStrings("hello", "goodbye")
    val stringArray = arrayOf("Frodo", "Sam", "Pippin")

    /** You can't add Kotlin array to Java array parameter without spread(*) operator */
    getCar().printStrings(*stringArray)
}

fun callStatic() {
    println(JavaCar.getCount())
}

/**
 * SAM(Single Abstract Method) in action.
 * We could pass lambda as a parameter in Java method arguments
 */
fun sam() {
    JavaCar.startNewThread { println("I'm in a new thread!") }
}

fun getCar() = JavaCar("Audi", "Blue", 2008)