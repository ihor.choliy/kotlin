/** Add unique name for Kotlin class, you should add this annotation above the package */
@file:JvmName("UniqueName")

package net.wizapps.kotlin.n_JavaInteroperability

import java.io.IOException

fun topLevelFunction() = println("I'm in the top level function")

/** @JvmField annotation gives a possibility to call not private fields from Java code without getters and setters */
class Avatar(var name: String, race: String, @JvmField var level: Int, var isMale: Boolean) {

    companion object {

        const val constant = 25
        @JvmField // this variable would be available without calling Companion object
        var hasIllnesses: Boolean = false
        var hasCurses: Boolean = false

        fun companionFunction() = println("I'm in the companion object")

        /**
         * If you want to call function in companion object from Java code without calling Companion object,
         * like that -> Avatar.Companion.jvmStaticFunction()
         * you should add @JvmStatic annotation before this function
         * and call like that -> Avatar.jvmStaticFunction()
         */
        @JvmStatic
        fun jvmStaticFunction() = println("I'm in the companion static function for Java calling")
    }

    var race: String = race
        set(value) {
            field = value
        }

    override fun toString(): String {
        return "Avatar(name='$name', level=$level, race='$race')"
    }
}

object KotlinSingleton {
    /**
     * If you don't want to call singleton class like in Java, from INSTANCE of the class,
     * like that -> KotlinSingleton.INSTANCE.singletonFunction()
     * you could add @JvmStatic annotation before this function, like in Companion object
     */
    @JvmStatic
    fun singletonFunction() = println("I'm in the Kotlin singleton class")
}

/**
 * If you want to catch exception that throws from Kotlin in Java,
 * you should inform JVM that this function throws current exception -> @Throws(IOException::class)
 * Otherwise in java try/catch block won't be an error, that in this function is thrown an exception
 */
@Throws(IOException::class)
fun throwsIO() {
    throw IOException()
}

/**
 * We have number with default value.
 * If we want to use two different overloaded functions from Java,
 * we should add @JvmOverloads, otherwise JVM generates only function with two parameters
 */
@JvmOverloads
fun printArgs(text: String, number: Number = 25) {
    println("Text: $text")
    println("Number: $number")
}