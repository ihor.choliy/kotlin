package net.wizapps.kotlin.f_Functions

import net.wizapps.kotlin.e_DataClasses.Car

fun main(args: Array<String>) {
    println("Some text".addLol())
    println(54.iterate())

    val car = Car("Audi", "White", 2008)
    car.changeYear(2017)
    println(car.year)
}

/**
 * With Kotlin extension functions you could add your own functionality
 * for any type of objects in Kotlin, Java or your oun library
 */

/** String extension sample */
fun String.addLol(): String {
    return "$this LoL"
}

/** Int extension sample */
fun Int.iterate(): Int {
    return this + 1
}

/** Object extension sample */
fun Car.changeYear(year: Int) {
    this.year = year
}