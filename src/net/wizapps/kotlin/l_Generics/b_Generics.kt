package net.wizapps.kotlin.l_Generics

fun main(args: Array<String>) {
    val doubles = listOf(23.3, 53.35, 7.4, 9.1, 3.024)
    println(convertToInt(doubles))
    append(StringBuilder("Ihor "), StringBuilder("Choliy"))
    printAllCollection(doubles) // this function didn't accept nullable type
    genericsTypeChecking()
}

/**
 * If you want to accept only objects witch extends concrete class,
 * you should specify like that <T : Number>
 */
fun <T : Number> convertToInt(collection: List<T>): List<Int> {
    val intCollection = mutableListOf<Int>()
    collection.forEach { intCollection.add(it.toInt()) }
    return intCollection
}

/**
 * If you want to specify more concrete type of object.
 * T should implement interfaces CharSequence & Appendable.
 * It's means that only StringBuilder and similar objects will be accepted
 */
fun <T> append(itemOne: T, itemTwo: T) where T : CharSequence, T : Appendable {
    println(itemOne.append(itemTwo))
}

/**
 * If you want to avoid nullable collection.
 * Extend generic type from Any object <T : Any>
 */
fun <T : Any> printAllCollection(collection: List<T>) = collection.forEach { println(it) }

fun genericsTypeChecking() {
    val ints = mutableListOf(1, 2, 3, 4, 5)

    /** Kotlin compiler unlike in Java, could understand generics type at runtime */
    if (ints is List<Int>) {
        println("This is Ints List")
    }

    val listAny: Any = listOf("1", "2", "3", "4", "5")

    /** If you don't know what type of list is it, you should add star projection "*" */
    if (listAny is List<*>) {
        println("This is a List. But unknown type")
    }
}