package net.wizapps.kotlin.l_Generics

fun main(args: Array<String>) {

    /** Add changes to Human class for successful compiling in 7th line */
    val joe = Human("Joe", "Jones", 45)
    val (firstName, lastName, age) = joe
    println("first name = $firstName, last name = $lastName, age = $age")

    /** Create a immutable map with key -> lastName, value -> class object */
    val jane = Human("Jane", "Smith", 32)
    val mary = Human("Mary", "Wilson", 25)
    val john = Human("John", "Adams", 47)
    val humanMap = mapOf(
            jane.lastName to jane,
            mary.lastName to mary,
            john.lastName to john)

    /** Prints count of people which first name starts with "J", without explicit loop */
    println(humanMap
            .filter { it.value.firstName.startsWith("J") }
            .count())

    /** Create a list of pairs from "humanMap" without a loop */
    // Faster way
    val pairsList = humanMap.map { Pair(it.value.firstName, it.value.lastName) }
    println(pairsList)

    // Longer way
    val firstNames = humanMap.map { it.value.firstName }
    val lastNames = humanMap.map { it.value.lastName }
    val pairList = firstNames.zip(lastNames) // zip function merge two lists by ids(0 with 0, 1 with 1 eth)
    println(pairList)

    /** Use "also()" collection function for printing: Jane is 32 years old */
    humanMap.also { it -> it.map { println("${it.value.firstName} is ${it.value.age} years old") } }

    /** Create third list witch contains all numbers that match in previous two, without looping */
    val numbersOne = listOf(3, 56, 22, 77, 1, 9, 7)
    val numbersTwo = listOf(22, 7, 3, 75, 13, 19, 1)

    // First way
    val numbersThree = numbersOne.filter { it in numbersTwo }
    println(numbersThree)

    // Second way
    val numberThree = numbersOne.filter { numbersTwo.contains(it) }
    println(numberThree)

    /** Make available assignment "regularPaper" to "paper" */
    var paper = Box<Paper>()
    val regularPaper = Box<RegularPaper>()
    paper = regularPaper
}

/** Or we could add "data" class modifier, which has this realization under the cover */
class Human(val firstName: String, val lastName: String, val age: Int) {
    operator fun component1() = firstName
    operator fun component2() = lastName
    operator fun component3() = age
}

open class Paper
class RegularPaper : Paper()
class PremiumPaper : Paper()
class Box<out T> // we should add out position to maker available such assigning