package net.wizapps.kotlin.l_Generics

fun main(args: Array<String>) {

    /** Creating Rose garden */
    val roseCare = object : FlowerCare<Rose> {
        override fun cutFlower(flower: Rose) {
            println("I'm cutting a rose!")
        }
    }
    val roseGarden = NewGarden(listOf(Rose(), Rose()), roseCare)
    println(roseGarden.pickFlower(1))
    roseGarden.cutFlower(0)

    /** Creating Chamomile garden */
    val chamomileCare = object : FlowerCare<Chamomile> {
        override fun cutFlower(flower: Chamomile) {
            println("I'm cutting a chamomile!")
        }
    }
    val chamomileGarden = NewGarden(listOf(Chamomile(), Chamomile()), chamomileCare)
    println(chamomileGarden.pickFlower(0))
    chamomileGarden.cutFlower(1)

    /**
     * To avoiding creating almost the same anonymous classes for FlowerCare,
     * we add "in" position(Contravariance) in generic declaration of FlowerCare<in T>,
     * this means that we cold add any superclass of Rose or Chamomile in generic type
     */
    val flowerCare = object : FlowerCare<Flower> {
        override fun cutFlower(flower: Flower) {
            println("I'm cutting a ${flower.name}")
        }
    }
    val newRoseGarden = NewGarden(listOf(Rose(), Rose()), flowerCare)
    val newChamomileGarden = NewGarden(listOf(Chamomile(), Chamomile()), flowerCare)
    newRoseGarden.cutFlower(0)
    newChamomileGarden.cutFlower(newRoseGarden.flowers.lastIndex)
}

/** Contravariance type of generic(in position) */
interface FlowerCare<in T> {
    fun cutFlower(flower: T)
}

class NewGarden<T : Flower>(val flowers: List<T>, private val flowerCare: FlowerCare<T>) {
    fun pickFlower(id: Int): T = flowers[id]
    fun cutFlower(id: Int) = flowerCare.cutFlower(flowers[id])
}