package net.wizapps.kotlin.l_Generics

fun main(args: Array<String>) {
    val floatList = listOf(12.5f, 654.3f, 5.23f, 3.2f)
    covariantConvert(floatList)

    /**
     * Mutable list is covariant,
     * that's why you can't add as a parameter subtype of "Number"
     */
    val doubleList = mutableListOf(15.5, 265.23, 8.5, 125.36)
    //invariantConvert(doubleList)
}

/**
 * Immutable lists in covariant.
 * That's why you could add any immutable list with subtype of "Number" (Short, Int etc.)
 */
fun covariantConvert(collection: List<Number>) {
    collection.forEach { println(it.toInt()) }
}

/**
 * All mutable lists is contravariant.
 * That's why you couldn't add any mutable list with subtype of "Number" (Short, Int etc.),
 * only mutable list of Numbers
 */
fun invariantConvert(collection: MutableList<Number>) {
    collection.forEach { println(it.toInt()) }
}