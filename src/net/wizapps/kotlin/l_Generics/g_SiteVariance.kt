package net.wizapps.kotlin.l_Generics

fun main(args: Array<String>) {

    val fords = mutableListOf(Ford(), Ford(), Ford())
    val cars = mutableListOf(BaseCar(), BaseCar())
    copyCars(fords, cars)
}

/**
 * You could combine Covariance(out position) and Contravariance(in position) in one function or class.
 * Kotlin -> MutableList<out T> | Java -> List<? extends BaseCar>
 * Kotlin -> MutableList<in T> | Java -> List<? super BaseCar>
 */
fun <T : BaseCar> copyCars(source: MutableList<out T>, copiedList: MutableList<in T>) {
    source.forEach { copiedList.add(it) }
}

open class BaseCar
class Ford : BaseCar()
class Toyota : BaseCar()